'use strict';

/**
 *  neuigkeit controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::neuigkeit.neuigkeit');
