'use strict';

/**
 * neuigkeit router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::neuigkeit.neuigkeit');
