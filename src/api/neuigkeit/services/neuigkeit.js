'use strict';

/**
 * neuigkeit service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::neuigkeit.neuigkeit');
