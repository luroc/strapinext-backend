'use strict';

/**
 * kunst-serie service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::kunst-serie.kunst-serie');
