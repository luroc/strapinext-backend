'use strict';

/**
 *  kunst-serie controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::kunst-serie.kunst-serie');
