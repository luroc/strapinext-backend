'use strict';

/**
 * kunst-serie router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::kunst-serie.kunst-serie');
